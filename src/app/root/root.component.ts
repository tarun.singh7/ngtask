import { Component } from '@angular/core';

import { FormControl, FormGroup} from '@angular/forms'
import {ThemePalette} from '@angular/material/core';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent {
  newNote = new FormGroup({
    noteColor : new FormControl(''),
    noteContent : new FormControl('')
  });

  newData : any;

  getData() {
    this.newData = this.newNote.value;
    this.newNote.controls.noteContent.setValue("");
    // console.log(this.newNote.value);
  }
}
