import { getLocaleDateFormat } from '@angular/common';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { CheckboxControlValueAccessor } from '@angular/forms';
import { combineAll, min } from 'rxjs';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnChanges{

  @Input() newDataChild:any
  noteStore: any[] = [];
  res: any[] = [];
  currNote : string = "";
  notempty : any = false;
  searchText : string = "";
  

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['newDataChild'].currentValue && changes['newDataChild'].currentValue.noteContent){
      // console.log(changes['newDataChild'].currentValue.noteContent);
      this.noteStore.push(changes['newDataChild'].currentValue);
      this.res.push(changes['newDataChild'].currentValue);
      // console.log(this.noteStore);
      this.notempty = true;
    }
  }

  delData(note : string, j : number) {
      this.res.splice(j,1);
      for(var i=0; i<this.noteStore.length; i++) {
        if(this.noteStore[i].noteContent == note) {
          this.noteStore.splice(i,1);
          break;
        }
      }
    }

  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.noteStore, event.previousIndex, event.currentIndex);
  }

  onKey(event : any) {
    this.searchText = event.target.value;
    if (this.searchText !== '') {
      //console.log(this.searchText)
      this.res = this.noteStore.filter(this.checkSub, this.searchText);
    } else {
      this.res = this.noteStore;
    }
  }

  checkSub(x : any) {
    return x.noteContent.includes(this);
  }

  editData(idx : any) {
    
  }
}


